/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reader;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.apache.xmlbeans.XmlObject;

import us.mtna.pojo.DataSet;
import us.mtna.reader.exceptions.ReaderException;

/**
 * Reader interface holds the getDataSet() methods used in the implementations
 * getDataSets takes a File and returns a list of DataSets (from
 * openmetadata-model) obtained by comparing the data sets in the file
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public interface Reader {

	/**
	 * Gets the data sets from a file.
	 * 
	 * @param file
	 *            the file
	 * @return the data sets
	 * @throws ReaderException
	 *             if an error occurs reading the file
	 */
	List<DataSet> getDataSets(File file);

	/**
	 * Gets the data sets from an input stream
	 * 
	 * @param input
	 *            the input stream to read
	 * @return the data sets
	 * @throws ReaderException
	 *             if an error occurs reading the stream
	 */
	List<DataSet> getDataSets(InputStream input);

	/**
	 * Gets the data sets from an XML Bean {@link XmlObject} representing the
	 * complete XML Document.
	 * 
	 * @param xml
	 *            the input XML document to read
	 * @return the data sets
	 * @throws ReaderException
	 *             if an error occurs reading the stream
	 */
	List<DataSet> getDataSets(XmlObject xml) ;

	/**
	 * Gets the data set from the input stream and sets the script name - to be
	 * used with the api implementations which can specify a position.
	 * 
	 * @param xml
	 * @param scriptName
	 * @param xmlName
	 * @param position
	 * @return
	 */
	DataSet getDataSets(InputStream xml, String scriptName, String xmlName, Integer position);
}
