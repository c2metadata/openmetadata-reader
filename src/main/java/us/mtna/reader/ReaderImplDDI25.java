/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlError;
import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.apache.xmlbeans.XmlOptions;
import org.ddialliance.ddi_2_5.xml.xmlbeans.BaseElementType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CatgryType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CitationType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookDocument;
import org.ddialliance.ddi_2_5.xml.xmlbeans.CodeBookType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DataDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.DimensnsType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.FileDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.FileTxtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.LablType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.LocationType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.QstnLitType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.QstnType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.SimpleTextType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.StdyDscrType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.SumStatType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.TitlStmtType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.VarFormatType;
import org.ddialliance.ddi_2_5.xml.xmlbeans.VarType;
import org.slf4j.LoggerFactory;

import us.mtna.data.transform.command.object.FileDetails;
import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.DataType;
import us.mtna.pojo.SummaryStatistics;
import us.mtna.pojo.Variable;
import us.mtna.reader.exceptions.ReaderException;
import us.mtna.reader.exceptions.XmlValidationException;
import us.mtna.reporting.simple.CodeSorter;

/**
 * Read a DDI 2.5 file, codebookDocument, or inputStream, and return a list of
 * datasets
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ReaderImplDDI25 implements Reader {

	private CodeBookType codebookType;
	private static final String FAILED_AT_STARTUP = "failed at startup";
	private Map<String, FileDscrType> fileDscrMap = new HashMap<>();
	private org.slf4j.Logger log = LoggerFactory.getLogger(ReaderImplDDI25.class);

	/**
	 * get a list of datasets from a file
	 */
	@Override
	public List<DataSet> getDataSets(File file) {
		try {
			return getDataSets(XmlObject.Factory.parse(file, new XmlOptions().setLoadStripWhitespace()));
		} catch (XmlException e) {
			log.error("xml exception", e);
			throw new ReaderException("Error reading XML", e);
		} catch (FileNotFoundException e) {
			log.error(FAILED_AT_STARTUP, e);
			throw new ReaderException("File not found", e);
		} catch (IOException e) {
			log.error(FAILED_AT_STARTUP, e);
			throw new ReaderException("IO error reading XML", e);
		}
	}

	// @Override
	public DataSet getDataSets(InputStream xml, String scriptName, String xmlName, Integer position) {

		try {
			XmlObject xmlObject = XmlObject.Factory.parse(xml, new XmlOptions().setLoadStripWhitespace());
			validate(xmlObject);
			List<DataSet> datasets = getDataSets(xmlObject);
			DataSet dataset = datasets.get(0);
			// position is one-based
			if (position != null && datasets.size() >= position) {
				dataset = datasets.get(position - 1);
			} else if (xmlName != null) {
				for (DataSet set : datasets) {
					if (set.getFileDscrName().equals(xmlName)) {
						dataset = set;
					}
				}
			}
			if (scriptName == null) {
				dataset.setScriptName(scriptName);
			}
			dataset.setScriptName(scriptName);
			return dataset;
		} catch (XmlException e) {
			log.error("xml exception", e);
			throw new ReaderException("Error reading XML", e);
		} catch (FileNotFoundException e) {
			log.error(FAILED_AT_STARTUP, e);
			throw new ReaderException("File not found", e);
		} catch (IOException e) {
			log.error(FAILED_AT_STARTUP, e);
			throw new ReaderException("IO error reading XML", e);
		}
	}

	@Override
	public List<DataSet> getDataSets(InputStream inputStream) {
		try {
			return getDataSets(XmlObject.Factory.parse(inputStream, new XmlOptions().setLoadStripWhitespace()));
		} catch (XmlException e) {
			log.error("xml exception", e);
			throw new ReaderException("Error reading XML", e);
		} catch (IOException e) {
			log.error(FAILED_AT_STARTUP, e);
			throw new ReaderException("IO error reading XML", e);
		}
	}

	@Override
	public List<DataSet> getDataSets(XmlObject xml) {
		if (!(xml instanceof CodeBookDocument)) {
			if (xml.schemaType().getName() != null) {
				log.error("Unsupported input document [" + xml.schemaType().getName().toString() + "]");
				throw new ReaderException("Unsupported input document [" + xml.schemaType().getName().toString() + "]");
			} else {
				log.error("Unsupported input document (No schema name information available.)");
				throw new ReaderException("Unsupported input document (No schema name information available.)");
			}
		}
		validate(xml);
		return getDataSets((CodeBookDocument) xml);
	}

	private void validate(XmlObject xml) {
		ArrayList<XmlError> errors = new ArrayList<>();
		xml.validate(new XmlOptions().setErrorListener(errors));
		if (!errors.isEmpty()) {
			throw new XmlValidationException(errors);
		}
	}

	/**
	 * get a list of datasets from a codebookDocument
	 * 
	 * @param cb
	 *            codeBookDocument
	 * @return list of datasets
	 */
	private List<DataSet> getDataSets(CodeBookDocument cb) {
		try {
			return parseDataSets(cb);
		} catch (ReaderException e) {
			log.error("reader exception", e);
			throw e;
		} catch (RuntimeException e) {
			log.error("unexpected exception", e);
			throw new ReaderException(e);
		}
	}

	/**
	 * parse and set dataset properties
	 * 
	 * @param codeDoc
	 *            codeBookDocument
	 * @return list of datasets
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	private List<DataSet> parseDataSets(CodeBookDocument codeDoc) {
		CodeBookType codeBook = codeDoc.getCodeBook();
		if (codeBook == null) {
			log.error("Codebook is null when trying to parse datsaets.");
			throw new ReaderException("Document cannot be read as a DDI 2.5 codeBook");
		}
		setCodebookType(codeBook);

		List<String> fileDscrIds = getFileDscrList(codeBook);
		List<DataSet> dataSetList = getDataDscrType(codeBook, fileDscrIds);
		int recordCount = getRecordCount(codeBook);
		String name = getCodeBookName(codeBook);
		String nameWithId = appendIdToTitle(codeBook, name);
		// set as -1 bc the first ds will increase it to 0
		int x = -1;
		ZonedDateTime date = ZonedDateTime.now();
		List<String> names = createDataSetIds(codeBook);
		for (DataSet dataSet : dataSetList) {
			log.trace("reading/creating dataset [codebookId=" + dataSet.getCodebookId() + "]");
			x++;
			dataSet.setDataSetId(names.get(x));
			dataSet.setUploadTime(date);
			dataSet.setRecordCount(recordCount);
			dataSet.setName(nameWithId);
			dataSet.setCodebookId(codeBook.getID());
			dataSet.setCodebookName(name);
			dataSet.setFile(fillOutFileInfo(dataSet));
		}
		return dataSetList;
	}

	/**
	 * Fills out a fileDetail object for a dataset
	 * 
	 * @param dataSet
	 * @return a file detail with info on one dataset.
	 */
	private FileDetails fillOutFileInfo(DataSet dataSet) {
		FileDetails details = new FileDetails();
		details.setFileName(dataSet.getFileDscrName());
		details.setUri(fileDscrMap.get(dataSet.getFileDscrId()).getURI());
		return details;
	}

	/**
	 * Gets a list of file description ids, and puts the file descriptions in
	 * the map with fileDscrId as the key
	 * 
	 * @param codeBook
	 * @return list of file description ids for all fileDscrs in the codebook
	 */
	private List<String> getFileDscrList(CodeBookType codeBook) {
		List<String> fileDescrIds = new ArrayList<>();
		for (FileDscrType fileDscrType : codeBook.getFileDscrList()) {
			fileDescrIds.add(fileDscrType.getID());
			fileDscrMap.put(fileDscrType.getID(), fileDscrType);
		}
		return fileDescrIds;
	}

	/**
	 * get the record count from the CaseQuantity element
	 * 
	 * @param codeBook
	 * @return number of records
	 */
	private int getRecordCount(CodeBookType codeBook) {
		String recordCount;
		int recordCountInteger = 0;
		for (FileDscrType fileDscrType : codeBook.getFileDscrList()) {
			List<FileTxtType> fileTxtList = fileDscrType.getFileTxtList();
			if (fileTxtList != null && !fileTxtList.isEmpty()) {
				FileTxtType fileTxtType = fileTxtList.get(0);
				if (fileTxtType.getDimensns() != null) {
					DimensnsType dimens = fileTxtType.getDimensns();
					if (dimens.getCaseQntyList() != null && !dimens.getCaseQntyList().isEmpty()) {
						recordCount = getXmlTextValue(dimens.getCaseQntyList().get(0));
						recordCountInteger = Integer.parseInt(recordCount.trim());
						List<DataSet> dataSetList = null;
						List<String> fileDscrIds = getFileDscrList(codeBook);
						dataSetList = getDataDscrType(codeBook, fileDscrIds);
						for (DataSet dataSet : dataSetList) {
							dataSet.setRecordCount(recordCountInteger);
						}

					} else {
						log.debug("record count is null on fileDscrType [id=" + fileDscrType.getID() + "]");
					}
				}
			}
		}
		return recordCountInteger;
	}

	/**
	 * create a list of dataset ids in the format of "codebookId.fileDscrId" to
	 * make unique ids for files that may contain more than one dataset
	 * 
	 * @param codeBook
	 * @return list of dataset ids
	 */
	private List<String> createDataSetIds(CodeBookType codeBook) {
		List<String> fileDscrList = getFileDscrList(codeBook);
		String dsId;
		String codeBookId = "";
		List<String> idList = new ArrayList<>();
		if (codeBook != null && codeBook.getID() != null) {
			codeBookId = codeBook.getID();
		}

		for (String id : fileDscrList) {
			dsId = codeBookId.concat(".").concat(id);
			idList.add(dsId);
		}
		return idList;
	}

	/**
	 * Gets the codebook name (StudyDscr -> Citation ->TitleStatement)
	 * 
	 * @param codeBook
	 * @return
	 */
	private String getCodeBookName(CodeBookType codeBook) {
		String name = null;
		if (codeBook.getStdyDscrList() != null && !codeBook.getStdyDscrList().isEmpty()) {
			StdyDscrType stdyDscr = codeBook.getStdyDscrList().get(0);
			if (stdyDscr.getCitationList() != null && !stdyDscr.getCitationList().isEmpty()) {
				// Get the name from the citation/titlStmt/titl
				CitationType citation = stdyDscr.getCitationList().get(0);
				if (citation != null) {
					TitlStmtType titlStmt = citation.getTitlStmt();
					if (titlStmt != null) {
						SimpleTextType titl = titlStmt.getTitl();
						name = getXmlTextValue(titl);
					}
				}
			} else {
				log.debug("No citation list on StudyDscr [id=" + stdyDscr.getID() + "]");
			}
		} else {
			log.debug("No StdyDscrType on codebook [id=" + codeBook.getID() + "]");
		}
		return name;
	}

	/**
	 * append codebook id to its name in the format "id_name"
	 * 
	 * @param codeBook
	 * @param name
	 * @return String "id_name"
	 */
	private String appendIdToTitle(CodeBookType codeBook, String name) {
		// take title, get codebook id, prepend id to title
		String id = "";
		if (codeBook.getID() != null) {
			id = codeBook.getID();
		}
		StringBuilder sb = new StringBuilder(id);
		return sb.append("_" + name).toString();
	}

	/**
	 * get a list of datasets populated with variables and classifications from
	 * the dataDscrType
	 * 
	 * @param codeBook
	 * @param fileDscrIds
	 * @return a list of populated datasets
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	private List<DataSet> getDataDscrType(CodeBookType codeBook, List<String> fileDscrIds) {

		Long startPosition = 1L;
		LinkedList<DataSet> dataSets = new LinkedList<>();
		for (String fileDscrId : fileDscrIds) {
			DataSet dataSet = new DataSet();
			dataSet.setId(fileDscrId);
			LinkedList<Variable> variables = new LinkedList<>();
			Map<String, Classification> uniqueClassificationMap = new LinkedHashMap<>();
			Map<String, Classification> classifIdMap = new LinkedHashMap<>();

			for (DataDscrType dataDescriptionType : codeBook.getDataDscrList()) {
				for (VarType variableType : dataDescriptionType.getVarList()) {

					// check if the variable should be added to this dataset
					// based on the file description id.
					if (variableType.isSetFiles() && variableType.getFiles().contains(fileDscrId)) {
						Variable codebookVariable = new Variable();
						codebookVariable.setName(variableType.getName());
						codebookVariable.setLabel(getVariableLabel(variableType));

						String variableId = variableType.getID();
						codebookVariable.setId(variableId);

						setStartEndPositions(startPosition, variableType, codebookVariable);

						String storageWidth = getStorageWidth(variableType);
						codebookVariable.setTextStorageWidth(storageWidth);

						SummaryStatistics summaryStatistics = getSummaryStatistics(variableType);
						codebookVariable.setSummaryStatistics(summaryStatistics);

						String literalQuestionText = getQuestionText(variableType);
						codebookVariable.setQuestionText(literalQuestionText);

						DataType dataType = getDataType(variableType);
						codebookVariable.setDataType(dataType);

						// build the classification
						Classification classification = getClassificationFromVariable(variableType,
								uniqueClassificationMap);
						codebookVariable.setClassification(classification);

						// put the classification in a linked hash map with
						// its
						// id as the key
						classifIdMap.put(classification.getId(), classification);
						variables.add(codebookVariable);
					}

				}
			}
			dataSet.setFileDscrId(fileDscrId);
			FileDscrType fileDscr = fileDscrMap.get(fileDscrId);
			if (fileDscr.getFileTxtList() != null && !fileDscr.getFileTxtList().isEmpty()) {
				FileTxtType fileTxt = fileDscr.getFileTxtList().get(0);
				if (fileTxt.getFileNameList() != null && !fileTxt.getFileNameList().isEmpty()) {
					dataSet.setFileDscrName(getXmlTextValue(fileTxt.getFileNameList().get(0)));
				}
			}
			dataSet.getMetadata().setClassifs(classifIdMap);
			dataSet.getMetadata().setVariables(variables);
			dataSets.add(dataSet);
		}
		return dataSets;
	}

	/**
	 * get a varType from its id
	 * 
	 * @param variableId
	 * @return
	 */
	private VarType getVarTypeFromId(String variableId) {
		for (DataDscrType dataDescriptionType : codebookType.getDataDscrList()) {
			for (VarType variableType : dataDescriptionType.getVarList()) {
				if (variableType.getID().equals(variableId)) {
					return variableType;
				}
			}
		}
		return null;
	}

	/**
	 * populates a {@link SummaryStatistics} object from a VarType
	 * 
	 * @param varType
	 * @return
	 */
	private SummaryStatistics getSummaryStatistics(VarType varType) {
		SummaryStatistics summaryStatistics = new SummaryStatistics();
		List<SumStatType> ssList = varType.getSumStatList();
		if (!ssList.isEmpty()) {
			Set<String> weightedVariableNames = new HashSet<>();

			for (SumStatType sst : ssList) {
				if (sst.getType() != null) {

					String summaryStatisticsValue = getXmlTextValue(sst);
					double summaryStatisticsDouble = parseStringToDouble(summaryStatisticsValue);
					SumStatType.Type.Enum type = sst.getType();
					boolean weighted = false;
					List<String> weightedVariableIds = new ArrayList<>();

					if (sst.getWgtd() != null) {

						// .getWeight() and getWgtVar() get the same thing and
						// are optional, so check both.
						List<?> wgtd = sst.getWeight();
						List<?> weightedVars = sst.getWgtVar();

						if (wgtd != null) {
							// resolve the names of the weighted variables by
							// their ids
							for (Object weightVariableId : wgtd) {
								weightedVariableIds.add((String) weightVariableId);
								VarType varTypeFomId = getVarTypeFromId((String) weightVariableId);
								if (varTypeFomId != null && varTypeFomId.getName() != null) {
									weightedVariableNames.add(varTypeFomId.getName());
								}
							}
						}
						if (weightedVars != null) {
							// resolve the names of the weighted variables by
							// their ids
							for (Object weightVariableId : weightedVars) {
								weightedVariableIds.add((String) weightVariableId);
								VarType varTypeFomId = getVarTypeFromId((String) weightVariableId);
								if (varTypeFomId != null && varTypeFomId.getName() != null) {
									weightedVariableNames.add(varTypeFomId.getName());
								}
							}
						}
					}
					// set a list of weighted variables on the sumstats object.
					// for DDI2.5 there will be only one
					summaryStatistics.setWeightedVariableNames(weightedVariableNames);
					// check if the value is weighted
					if (sst.getWgtd() != null) {
						SumStatType.Wgtd.Enum wgtd = sst.getWgtd();
						if (wgtd.intValue() == 1) {
							weighted = true;
						} else {
							weighted = false;
						}
					}
					setSumStatsMeasures(summaryStatistics, summaryStatisticsDouble, type, weighted);
				}
			}
		}
		return summaryStatistics;
	}

	/**
	 * check if value is weighted, then set the correct measure on the
	 * SummaryStatistics object.
	 * 
	 * @param summaryStatistics
	 * @param summaryStatisticsDouble
	 * @param type
	 * @param weighted
	 */
	private void setSumStatsMeasures(SummaryStatistics summaryStatistics, double summaryStatisticsDouble,
			SumStatType.Type.Enum type, boolean weighted) {
		switch (type.intValue()) {
		case 1:
			if (weighted) {
				summaryStatistics.setWeightedMean(summaryStatisticsDouble);
			} else {
				summaryStatistics.setMean(summaryStatisticsDouble);
			}
			break;
		case 2:
			if (weighted) {
				summaryStatistics.setWeightedMedian(summaryStatisticsDouble);
			} else {
				summaryStatistics.setMedian(summaryStatisticsDouble);
			}
			break;
		case 3:
			if (weighted) {
				summaryStatistics.setWeightedMode(summaryStatisticsDouble);
			} else {
				summaryStatistics.setMode(summaryStatisticsDouble);
			}
			break;
		case 4:
			if (weighted) {
				summaryStatistics.setWeightedValid(summaryStatisticsDouble);
			} else {
				summaryStatistics.setValid(summaryStatisticsDouble);
			}
			break;
		case 5:
			if (weighted) {
				summaryStatistics.setWeightedInvalid(summaryStatisticsDouble);
			} else {
				summaryStatistics.setInvalid(summaryStatisticsDouble);
			}
			break;
		case 6:
			if (weighted) {
				summaryStatistics.setWeightedMin(summaryStatisticsDouble);
			} else {
				summaryStatistics.setMin(summaryStatisticsDouble);
			}
			break;
		case 7:
			if (weighted) {
				summaryStatistics.setWeightedMax(summaryStatisticsDouble);
			} else {
				summaryStatistics.setMax(summaryStatisticsDouble);
			}
			break;
		case 8:
			if (weighted) {
				summaryStatistics.setWeightedStdDev(summaryStatisticsDouble);
			} else {
				summaryStatistics.setStdDev(summaryStatisticsDouble);
			}
			break;
		default:
			if (weighted) {
				summaryStatistics.setWeightedOther(summaryStatisticsDouble);
			} else {
				summaryStatistics.setOther(summaryStatisticsDouble);
			}
			break;
		}
	}

	private static final double parseStringToDouble(String value) {
		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException nfe) {
			return 0.0;
		}
	}

	/**
	 * Returns the trimmed string value of the first element in the label list
	 * on the varType
	 * 
	 * @param varType
	 * @return variable label
	 */
	private String getVariableLabel(VarType varType) {
		String label = null;
		if (varType.getLablList().size() > 0) {
			LablType labelType = varType.getLablArray(0);
			label = getXmlTextValue(labelType);
			label = label.replaceAll("(\\r|\\n)", "").trim();
		}
		return label;
	}

	private String getStorageWidth(VarType varType) {
		String width = null;
		String decimals = "0";
		if (varType.getDcml() != null) {
			decimals = varType.getDcml();
		}
		width += ("." + decimals);
		return width;
	}

	private String getQuestionText(VarType varType) {
		String literalQuestionText = null;
		if (varType.getQstnList() != null && !varType.getQstnList().isEmpty()) {
			QstnType qstn = varType.getQstnList().get(0);
			if (qstn.getQstnLitList() != null && !qstn.getQstnLitList().isEmpty()) {
				QstnLitType qstnLit = qstn.getQstnLitList().get(0);
				literalQuestionText = getXmlTextValue(qstnLit);
			}
		} else {
			// log.info("No QuestionText on VarType [id=" + varType.getID() +
			// "]");
		}
		return literalQuestionText;
	}

	/**
	 * sets the start and end position on the variable
	 * 
	 * @param startPosition
	 * @param varType
	 * @param variable
	 */
	private void setStartEndPositions(Long startPosition, VarType varType, Variable variable) {
		String width;
		if (varType.getLocationList() != null && !varType.getLocationList().isEmpty()) {
			LocationType location = varType.getLocationArray(0);
			if (location.getWidth() != null) {
				width = location.getWidth();
				variable.setTextDisplayWidth(Integer.parseInt(width));
			} else {
				log.debug("No width value on location [id=" + location.getID() + "]");
			}
			if (location.getStartPos() != null) {
				variable.setOriginalStart(Long.parseLong(location.getStartPos()));
				variable.setStartPosition(Long.parseLong(location.getStartPos()));
			} else {
				variable.setOriginalStart(startPosition);
				variable.setStartPosition(startPosition);
			}
			if (location.getEndPos() != null) {
				variable.setOriginalEnd(Long.parseLong(location.getEndPos()));
				variable.setEndPosition(Long.parseLong(location.getEndPos()));
			} else {
				Long end = startPosition + (Long.parseLong(location.getWidth()) - 1);
				variable.setOriginalEnd(end);
				variable.setEndPosition(end);
				if (end == startPosition) {
					startPosition++;
					variable.setStartPosition(startPosition);
				} else {
					startPosition = end + 1;
					variable.setStartPosition(startPosition);
				}
			}
		}
	}

	private DataType getDataType(VarType var) {
		VarFormatType varFormat = var.getVarFormat();
		if (varFormat == null) {
			return DataType.STRING;
		}
		VarFormatType.Type.Enum type = varFormat.getType();

		if (type != null && ("date").equalsIgnoreCase(type.toString())) {
			return DataType.DATE;
		} else if (type != null && ("boolean").equalsIgnoreCase(type.toString())) {
			return DataType.BOOLEAN;
		} else if (type != null && ("numeric").equalsIgnoreCase(type.toString())) {
			return DataType.NUMBER;
		} else {
			return DataType.STRING;
		}
	}

	/**
	 * gets a classification populated with the codes contained in the varType
	 * 
	 * @param var
	 * @return classification with codes set on it
	 */
	private Classification getClassification(VarType var) {
		Classification classification = new Classification();
		List<Code> classifCodeList = populateCodeListFromCat(var);
		classification.setCodeList(classifCodeList);
		return classification;
	}

	/**
	 * Creates a new classification from the varType's categories, then creates
	 * a unique hash from the codelist. if this hash is unique to the dataset so
	 * far, use it. If the hash already exists, use the original classification
	 * associated with that hash.
	 * 
	 * @param var
	 * @param uniqueClassMap
	 * @return harmonized classification
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	private Classification getClassificationFromVariable(VarType var, Map<String, Classification> uniqueClassMap) {
		// new classif populated from varType's categories
		Classification variableClassification = getClassification(var);
		// hash of codelist
		String classificationHash = getClassificationHash(variableClassification);
		// use the classification hash to get the classification from the unique
		// classif map
		Classification classification = uniqueClassMap.get(classificationHash);
		// if there's no classification for this hash, put it in the unique
		// classif map and set it to the variable's classification
		if (classification == null) {
			uniqueClassMap.put(classificationHash, variableClassification);
			classification = variableClassification;
		}
		return classification;
	}

	/**
	 * Turns the classification's sorted code list into a hashed string
	 * 
	 * @param classification
	 * @return hashed string of code list
	 * @throws UnsupportedEncodingException
	 * @throws NoSuchAlgorithmException
	 */
	private String getClassificationHash(Classification classification) {
		List<Code> codeList = classification.getCodeList();
		Collections.sort(codeList, new CodeSorter());

		StringBuilder sb = new StringBuilder();
		for (Code code : codeList) {
			sb.append(code.getCodeValue());
			if (code.getLabel() != null) {
				sb.append(code.getLabel().toLowerCase());
			}
		}
		String codeString = sb.toString();
		try {
			byte[] bytesOfMessage = codeString.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hashedString = md.digest(bytesOfMessage);
			return new String(hashedString);
		} catch (UnsupportedEncodingException e) {
			throw new ReaderException("UTF-8 encoding is not supported by environment", e);
		} catch (NoSuchAlgorithmException e) {
			throw new ReaderException("MD5 is not supported by environment", e);

		}
	}

	/**
	 * returns a list of codes with label, value, and missing status set on them
	 * from the varType's categories
	 * 
	 * @param var
	 * @return list of codes
	 */
	private List<Code> populateCodeListFromCat(VarType var) {
		List<Code> classifCodeList = new ArrayList<>();
		for (CatgryType cat : var.getCatgryList()) {
			Code code = new Code();
			code.setCodeValue(getXmlTextValue(cat.getCatValu()));
			if (cat.getLablList() != null && !cat.getLablList().isEmpty()) {
				code.setLabel(getXmlTextValue(cat.getLablList().get(0)));
			}
			if (cat.isSetMissing()) {
				code.setMissing(true);
			}
			// categories have a list of labels. but here we're just assigning
			// the first one to be the code label, make sure this isn't a
			// problem;
			classifCodeList.add(code);
		}
		return classifCodeList;
	}

	/**
	 * utility method to get the text value of any element using the XmlCursor
	 * 
	 * @param element
	 * @return string value of xml element
	 */
	private String getXmlTextValue(BaseElementType element) {
		XmlCursor curs = element.newCursor();
		String text = curs.getTextValue();
		curs.dispose();
		return text;
	}

	public CodeBookType getCodebookType() {
		return codebookType;
	}

	public void setCodebookType(CodeBookType codebookType) {
		this.codebookType = codebookType;
	}

	public void setLog(org.slf4j.Logger log) {
		this.log = log;
	}
}
