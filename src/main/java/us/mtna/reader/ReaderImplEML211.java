package us.mtna.reader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlObject;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.AttributeListType;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.AttributeType;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.AttributeType.MeasurementScale;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.AttributeType.MissingValueCode;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.NonNumericDomainType;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.NonNumericDomainType.EnumeratedDomain;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.NonNumericDomainType.EnumeratedDomain.CodeDefinition;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.NonNumericDomainType.EnumeratedDomain.EntityCodeList;
import org.ecoinformatics.eml_2_1_1.attribute.xml.xmlbeans.NonNumericDomainType.TextDomain;
import org.ecoinformatics.eml_2_1_1.dataset.xml.xmlbeans.DatasetType;
import org.ecoinformatics.eml_2_1_1.datatable.xml.xmlbeans.DataTableType;
import org.ecoinformatics.eml_2_1_1.xml.xmlbeans.EmlDocument;
import org.slf4j.LoggerFactory;

import us.mtna.data.transform.command.object.FileDetails;
import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.pojo.Variable;
import us.mtna.reader.exceptions.ReaderException;

public class ReaderImplEML211 implements Reader {

	private org.slf4j.Logger log = LoggerFactory.getLogger(ReaderImplEML211.class);
	private String FAILED__AT_STARTUP = "failed at startup";
	private Map<String, DataTableType> dataTableMap = new HashMap<>();
	// maps for looking up entities referenced for code lists.
	// entity id to data table type
	private Map<String, DataTableType> entityMap = new HashMap<>();
	// entity id to list of attribute types it contains
	private Map<String, AttributeType> attributeMap = new HashMap<>();
	private Map<String, List<Code>> attributeCodeMap = new HashMap<>();

	@Override
	public DataSet getDataSets(InputStream xml, String scriptName, String xmlName, Integer position) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<DataSet> getDataSets(File file) {
		try {
			return getDataSets(XmlObject.Factory.parse(file));
		} catch (XmlException e) {
			log.error("xml exception", e);
			throw new ReaderException("Error reading XML", e);
		} catch (FileNotFoundException e) {
			log.error(FAILED__AT_STARTUP, e);
			throw new ReaderException("File not found", e);
		} catch (IOException e) {
			log.error(FAILED__AT_STARTUP, e);
			throw new ReaderException("IO error reading XML", e);
		}
	}

	@Override
	public List<DataSet> getDataSets(InputStream inputStream) {
		try {
			return getDataSets(XmlObject.Factory.parse(inputStream));
		} catch (XmlException e) {
			log.error("xml exception", e);
			throw new ReaderException("Error reading XML", e);
		} catch (IOException e) {
			log.error(FAILED__AT_STARTUP, e);
			throw new ReaderException("IO error reading XML", e);
		}
	}

	// TODO I think this check and casting should be done before this point,
	// when you're deciding which reader to use.
	//try to redirect to the other reader?
	@Override
	public List<DataSet> getDataSets(XmlObject xml)  {
		if (!(xml instanceof EmlDocument)) {
			if (xml.schemaType().getName() != null) {
				log.error("Unsupported input document [" + xml.schemaType().getName().toString() + "]");
				throw new ReaderException("Unsupported input document [" + xml.schemaType().getName().toString() + "]");
			} else {
				log.error("Unsupported input document (No schema name information available.)");
				throw new ReaderException("Unsupported input document (No schema name information available.)");
			}
		}
		return parseDataSets((EmlDocument) xml);
	}

	private List<DataSet> parseDataSets(EmlDocument emlDoc) {
		List<DataSet> datasets = new ArrayList<>();
		if (emlDoc == null) {
			log.error("EmlDocument is null when trying to parse datsaets.");
			throw new ReaderException("Document cannot be read as an EML 2.1.1 Document");
		}

		DatasetType dataSetType = emlDoc.getEml().getDataset();

		for (DataTableType dataTable : dataSetType.getDataTableList()) {
			DataSet dataset = new DataSet();
			populateDatasetMetadata(dataset, dataTable);
			dataTableMap.put(dataTable.getEntityName(), dataTable);
			// set version based on the schema version listed in the eml
			// element?

			AttributeListType attributeList = dataTable.getAttributeList();
			Map<String, Classification> classifications = new HashMap<>();
			for (AttributeType attribute : attributeList.getAttributeList()) {
				Variable variable = new Variable();
				variable.setName(attribute.getAttributeName());
				variable.setDescription(attribute.getAttributeDefinition());
				dataset.getMetadata().addVariables(variable);
				Classification classification = new Classification();

				if (attribute.getMeasurementScale() != null) {
					MeasurementScale scale = attribute.getMeasurementScale();
					if (scale.getNominal() != null) {
						if (scale.getNominal().getNonNumericDomain() != null) {
							NonNumericDomainType nonNumericDomain = scale.getNominal().getNonNumericDomain();
							getEnumeratedDomains(classifications, variable, nonNumericDomain);
							getTextDomains(classification, nonNumericDomain);
							if (nonNumericDomain.getReferences() != null) {
								// don't think we can resolve references.
							}
						}
						// TODO maybe pass in classification to set a
						// description on it for non enumerated domains?
						checkOtherScaleTypes(classification, scale);
					}
				}
				getMissingCodes(attribute, classification);
				if (populated(classification)) {
					classifications.put(classification.getId(), classification);
					variable.setClassification(classification);
					variable.setClassificationId(classification.getId());
				}

			}
			dataset.getMetadata().setClassifs(classifications);
			datasets.add(dataset);
		}
		return datasets;
	}

	/**
	 * Check if the classification has been populated with codes or had name
	 * information set on it, to see if it should be added to the metadata list.
	 * 
	 * @param classification
	 * @return
	 */
	private boolean populated(Classification classification) {
		return (classification.getName() != null || !classification.getCodeList().isEmpty());
	}

	/**
	 * Checks if the attribute has a missing value code list, and if so, creates
	 * and adds
	 * 
	 * @param attributeType
	 * @param classification
	 */
	private void getMissingCodes(AttributeType attributeType, Classification classification) {
		if (attributeType.getMissingValueCodeList() != null && !attributeType.getMissingValueCodeList().isEmpty()) {
			List<Code> missingCodes = new ArrayList<>();
			for (MissingValueCode missing : attributeType.getMissingValueCodeList()) {
				Code code = new Code();
				code.setCodeValue(missing.getCode());
				code.setLabel(missing.getCodeExplanation());
				code.setMissing(true);
				missingCodes.add(code);
			}
			classification.addCodesToCodeList(missingCodes);
		}
	}

	// delete? need anything from these? set a classif description ?
	private void checkOtherScaleTypes(Classification classification, MeasurementScale scale) {
		if (scale.getDateTime() != null) {
			if (scale.getDateTime().getFormatString() != null) {
				// get this value
			}
		}
		if (scale.getInterval() != null) {
			// scale.getInterval().getNumericDomain().get
		}
		if (scale.getOrdinal() != null) {

		}
		if (scale.getRatio() != null) {
			if (scale.getRatio().getUnit() != null) {
				if (scale.getRatio().getUnit().getStandardUnit() != null) {
					// get this value
				}
				if (scale.getRatio().getNumericDomain() != null) {
					// get this value
				}
			}
		}
	}

	private void getTextDomains(Classification classification, NonNumericDomainType nonNumericDomain) {
		if (nonNumericDomain.getTextDomainList() != null && !nonNumericDomain.getTextDomainList().isEmpty()) {
			for (TextDomain textDomain : nonNumericDomain.getTextDomainList()) {
				if (textDomain.getDefinition() != null) {
					// set classification description?
					textDomain.getDefinition();
				}
			}
		}
	}

	/**
	 * Enumerated Domains in EML can hold lists of codes. Here, we check if this
	 * nonNumericDomain contains a enumerated domain, and if so, create and
	 * populate a classification from it. If the enumerated domain contains a
	 * code list, new codes will be created from that specification. If an
	 * entityCode list is found, we will try to get the referenced codes from
	 * elsewhere in the file and add them to our classification.
	 * 
	 * @param classifications
	 * @param variable
	 * @param nonNumericDomain
	 */
	private void getEnumeratedDomains(Map<String, Classification> classifications, Variable variable,
			NonNumericDomainType nonNumericDomain) {
		if (nonNumericDomain.getEnumeratedDomainList() != null
				&& !nonNumericDomain.getEnumeratedDomainList().isEmpty()) {
			Classification classification = new Classification();
			variable.setClassificationId(classification.getId());
			for (EnumeratedDomain enumeratedDomain : nonNumericDomain.getEnumeratedDomainList()) {
				if (enumeratedDomain.getCodeDefinitionList() != null
						&& !enumeratedDomain.getCodeDefinitionList().isEmpty()) {
					for (CodeDefinition codeDefinition : enumeratedDomain.getCodeDefinitionList()) {
						Code code = new Code();
						code.setCodeValue(codeDefinition.getCode());
						code.setLabel(codeDefinition.getDefinition());
						classification.addNewCode(code);
					}
				}
				// get codes from another attribute. do
				if (enumeratedDomain.getEntityCodeList() != null) {
					EntityCodeList entityCodeList = enumeratedDomain.getEntityCodeList();
					// reference to the entity where the code was defined
					DataTableType dtt = entityMap.get(entityCodeList.getEntityReference());
					// make sure the attribute exists in the dtt?

					for (Code c : attributeCodeMap.get(entityCodeList.getValueAttributeReference())) {
						// get the value for this code
						Code newCode = new Code();
						newCode.setCodeValue(c.getCodeValue());
						//TODO is this code set anywhere? 
					}
					if (entityCodeList.getValueAttributeReference()
							.equals(entityCodeList.getDefinitionAttributeReference())) {
						for (Code c : attributeCodeMap.get(entityCodeList.getDefinitionAttributeReference())) {
							// harmonize and set the definition
						}
					}
					// would you always grab the whole list, or just specific
					// codes?

					// this ones optional - refers to another attribute holding
					// the code list
					entityCodeList.getOrderAttributeReference();

				}
				if (enumeratedDomain.getExternalCodeSet() != null) {
					// don't think we're going to try and resolve these.
					enumeratedDomain.getExternalCodeSet().getCodesetName();
					enumeratedDomain.getExternalCodeSet().getCitationList(); // optional
					enumeratedDomain.getExternalCodeSet().getCodesetURLList(); // optional
				}

			}
			classifications.put(classification.getId(), classification);
		}

	}

	/**
	 * Populates the dataset level properties.
	 * 
	 * @param dataset
	 * @param dataTable
	 * @return the original dataset with the populated properties.
	 */
	public DataSet populateDatasetMetadata(DataSet dataset, DataTableType dataTable) {

		if (dataTable.getEntityName() != null) {
		//	dataset.setDataSetId(dataTable.getEntityName());
			//file dscr name is pretty much what's being matched on.  
			//is each data table an entity - 
			//could you use the sha authentication key here as a uri? 
			dataset.setFileDscrName(dataTable.getEntityName());
			//the physical.sha-authentication isn't required.
		}
		//this is too long to use in the name
//		if (dataTable.getEntityDescription() != null) {
//			dataset.setFileDscrName(dataTable.getEntityDescription());
//		}
		dataset.setFile(fillOutFileInfo(dataset));
		// dataset.setCodebookId(codebookId);
		// dataset.setCodebookName(codebookName);-->dataTable.entityDescription
		// dataset.setFileDscrId(fileDscrId);
		// dataset.setFileDscrName(fileDscrName);
		// dataset.setId(id);
		// dataset.setName(name);
		// dataset.setRecordCount(recordCount);
		// dataset.setRecords(records);
		dataset.setUploadTime(ZonedDateTime.now());
		return dataset;
	}

	private FileDetails fillOutFileInfo(DataSet dataSet) {
		FileDetails details = new FileDetails();
		details.setFileName(dataSet.getFileDscrName());
		// idk on this. getId() gets a list
		// details.setUri(dataTableMap.get(dataSet.getFileDscrId()).getEntityName());
		return details;
	}

	

}
