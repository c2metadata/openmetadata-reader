/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna.reader;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A utility to copy a POJO resource to a new instance.
 * 
 * @author Jack Gager (j.gager@mtna.us)
 *
 */
public class ResourceCopyUtility {

	/**
	 * If the global {@link ObjectMapper} is null we will return a default
	 * ObjectMapper that will have no modules / mixins / (de)serializers
	 * registered. This allows for a check to be put in place in the setter that
	 * prevents a custom OM from being overridden but does not prevent the
	 * utility from being used with no OM set.
	 */
	private static ObjectMapper OM;
	
	static {
		OM = new ObjectMapper();
		OM.findAndRegisterModules();
	}

	/**
	 * Copies the resource POJO into a new instance.
	 * 
	 * @param resourceClass
	 *            the class of the resource POJO
	 * @param resource
	 *            the resource to copy
	 * @return a full copy of the resource
	 */
	public static <T> T copyResource(Class<T> resourceClass, T resource) {
		try {
			PipedOutputStream out = new PipedOutputStream();
			PipedInputStream in = new PipedInputStream(out);
			new Thread() {
				@Override
				public void run() {
					try {
						OM.writeValue(out, resource);
					} catch (IOException e) {
						throw new RuntimeException("Map copying exception", e);
					}
				}
			}.start();
			return OM.readValue(in, resourceClass);
		} catch (IOException e) {
			throw new RuntimeException("Map copying exception", e);
		}
	}
	
	/**
	 * Setter for spring configuration.
	 * 
	 * @param objectMapper
	 *            the object mapper
	 */
	public static void setObjectMapper(ObjectMapper objectMapper) {
		OM = objectMapper;
	}

}
