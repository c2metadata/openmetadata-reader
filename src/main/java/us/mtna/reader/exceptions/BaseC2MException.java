package us.mtna.reader.exceptions;

/**
 * A base for all of our exceptions to extend
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class BaseC2MException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public BaseC2MException() {
		super();
	}

	public BaseC2MException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BaseC2MException(String message, Throwable cause) {
		super(message, cause);
	}

	public BaseC2MException(String message) {
		super(message);
	}

	public BaseC2MException(Throwable cause) {
		super(cause);
	}

}
