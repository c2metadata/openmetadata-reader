package us.mtna.reader.exceptions;

/**
 * A generic runtime exception for issue that occur reading a file into a data
 * set.
 * 
 * @author Jack Gager (j.gager@mtna.us)
 *
 */
public class ReaderException extends RuntimeException {

	private static final long serialVersionUID = -6678767091017446562L;

	public ReaderException(String message) {
		super(message);
	}

	public ReaderException(Throwable cause) {
		this("An unspecified error occured reading the file", cause);
	}

	public ReaderException(String message, Throwable cause) {
		super(message, cause);
	}

}
