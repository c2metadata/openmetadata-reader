package us.mtna.reader.exceptions;

import us.mtna.reader.exceptions.BaseC2MException;

/**
 * A base exception for specifying that the exception was caused by a bad
 * request on the user end.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class RequestException extends BaseC2MException {

	private static final long serialVersionUID = 1L;

	public RequestException() {
		super();
	}

	public RequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public RequestException(String message) {
		super(message);
	}

	public RequestException(String message, String suggestedFix) {
		super(message);
	}

	public RequestException(Throwable cause) {
		super(cause);
	}

}
