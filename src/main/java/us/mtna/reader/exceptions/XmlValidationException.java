package us.mtna.reader.exceptions;

import java.util.ArrayList;
import java.util.List;

import org.apache.xmlbeans.XmlError;

public class XmlValidationException extends RequestException {

	private List<XmlError> errors;
	private List<String> errorMessages;
	private static final long serialVersionUID = 1L;

	public XmlValidationException() {
		super();
	}

	public XmlValidationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public XmlValidationException(String message, Throwable cause) {
		super(message, cause);
	}

	public XmlValidationException(String message) {
		super(message);
	}

	public XmlValidationException(Throwable cause) {
		super(cause);
	}

	public XmlValidationException(ArrayList<XmlError> errors) {
		super();
		this.errorMessages = new ArrayList<>();
		this.errors = errors;
		for (XmlError e : errors) {
			if (e.getMessage() != null) {
				this.errorMessages.add(e.getMessage());
			}
		}
	}

	public List<XmlError> getErrors() {
		return errors;
	}

	public void setErrors(List<XmlError> errors) {
		this.errors = errors;
	}

	public List<String> getErrorMessages() {
		return errorMessages;
	}

	public void setErrorMessages(List<String> errorMessages) {
		this.errorMessages = errorMessages;
	}
}
