/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package us.mtna;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.pojo.Classification;
import us.mtna.pojo.DataSet;
import us.mtna.reader.ReaderImplDDI25;
import us.mtna.reader.ResourceCopyUtility;

public class DDI25Test {

	//private File file = new File("src/test/resources/ANES_1948_TS.DATA.ddi25.xml");
	private File file = new File("src/test/resources/georgeExample.xml");

	private ReaderImplDDI25 reader;
	private List<DataSet> ds;
	
	@Before
	public void setUp() {
		reader = new ReaderImplDDI25();
		ds = reader.getDataSets(file);
	}
	
	@Test
	public void testNumberOfVars(){
		DataSet ds0 = ds.get(0);
		int x = ds0.getMetadata().getVariables().size();
		assertEquals(67, x); 
	}
	
	@Test
	public void testNumberOfCodeLists(){
		DataSet ds0 = ds.get(0);
		int x = ds0.getMetadata().getClassifs().size();
		for(Classification classif : ds0.getMetadata().getClassifs().values()){
			System.out.println(classif.getId());
		}
		assertEquals(46, x);
	}
	
	@Test
	public void testNumberOfClassifIds(){
		DataSet ds0 = ds.get(0);
		int y = 0;
		System.out.println("+++++++++++  Classification IDs  ++++++++++++");

		for(String classifId : ds0.getMetadata().getClassifs().keySet()){
			y++;
			System.out.println(classifId);
		}
		System.out.println("+++++++++++  number of classification ids  ++++++++++++");
		System.out.println(y);
		assertEquals(46, y); 
	}
	
	@Test 
	public void testDataSets(){
		DataSet ds0 = ds.get(0);
		System.out.println("+++++++++++  Data Sets  ++++++++++++");
		System.out.println(ds0);
		
		assertEquals(1, ds.size()); 
	}

	@Test 
	public void testDataSetCopy() throws JsonGenerationException, JsonMappingException, IOException{
		DataSet ds0 = ds.get(0);
		DataSet ds1 = ResourceCopyUtility.copyResource(DataSet.class, ds0);
		assertEquals(ds0.getId(), ds1.getId());
	
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		System.out.println(mapper.writeValueAsString(ds1));
	}

}
