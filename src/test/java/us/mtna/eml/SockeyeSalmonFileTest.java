package us.mtna.eml;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import us.mtna.pojo.Classification;
import us.mtna.pojo.Code;
import us.mtna.pojo.DataSet;
import us.mtna.reader.ReaderImplEML211;
import us.mtna.reader.ResourceCopyUtility;

public class SockeyeSalmonFileTest {
	
	private File file = new File("src/test/resources/emlFiles/sockeyeSalmon.xml");
	private ReaderImplEML211 reader;
	private List<DataSet> dataSets;

	//TODO check all of these numbers.
	
	@Before
	public void setUp() {
		reader = new ReaderImplEML211();
		dataSets = reader.getDataSets(file);
	}

	@Test
	public void testNumberOfVars() {
		DataSet ds0 = dataSets.get(0);
		int x = ds0.getMetadata().getVariables().size();
		// maybe 27 with the second data table included?
		assertEquals(7, x);
	}

	@Test
	public void testNumberOfCodeLists() {
		DataSet ds0 = dataSets.get(0);
		int x = ds0.getMetadata().getClassifs().size();
		for (Classification classif : ds0.getMetadata().getClassifs().values()) {
			System.out.println(classif.getId());
		}
		// not sure of this number. right now just the same as vars.
		assertEquals(4, x);
	}

	@Test
	public void testNumberOfClassifIds() {
		DataSet ds0 = dataSets.get(0);
		int y = 0;
		System.out.println("+++++++++++  Classification IDs  ++++++++++++");

		for (String classifId : ds0.getMetadata().getClassifs().keySet()) {
			y++;
			System.out.println(classifId);
		}
		System.out.println("+++++++++++  number of classification ids  ++++++++++++");
		System.out.println(y);
		assertEquals(4, y);
	}

	@Test
	public void testMissingCodes() {
		// DataSet ds0 = ds.get(0);
		int x = 0;
		for (DataSet ds : dataSets) {
			for (Classification classif : ds.getMetadata().getClassifs().values()) {
				for (Code c : classif.getCodeList()) {
					if (c.isMissing()) {
						x++;
					}
				}
			}
		}
		System.out.println(x);
		assertTrue(x > 0);
	}

	@Test
	public void testDataSets() {
		System.out.println("+++++++++++  Data Sets  ++++++++++++");
		for (DataSet dataset : dataSets) {
			System.out.println(dataset.getDataSetId());
		}
		assertEquals(1, dataSets.size());
	}

	@Test
	public void testDataSetCopy() throws JsonGenerationException, JsonMappingException, IOException {
		DataSet ds0 = dataSets.get(0);
		DataSet ds1 = ResourceCopyUtility.copyResource(DataSet.class, ds0);
		assertEquals(ds0.getId(), ds1.getId());

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.setSerializationInclusion(Include.NON_EMPTY);

		mapper.findAndRegisterModules();
		System.out.println(mapper.writeValueAsString(ds1));
	}

	@Test
	public void printAllDataSets() throws JsonGenerationException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(Include.NON_NULL);
		mapper.setSerializationInclusion(Include.NON_EMPTY);

		mapper.findAndRegisterModules();
		for (DataSet dataSet : dataSets) {
			System.out.println(mapper.writeValueAsString(dataSet));

		}
	}
}
